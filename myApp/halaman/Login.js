import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image } from 'react-native';

class Login extends React.Component  {
    render(){
      const { navigation } = this.props;
      return (
        <View style={styles.container}>
          <Image
          source={require('./../assets/images/plaudern.png')}          
          style={styles.logo}
          />    
          <View style={{backgroundColor:'#e1d6f6', padding:50, alignItems:'center'}}>
            <Text style={[styles.teks, {alignItems:'center', color:'#8f00ff', fontWeight:'bold', fontSize:36, marginBottom:30}]}> LOGIN </Text>
            <TextInput
              style={styles.input}
              placeholder = "Email"
              placeholderTextColor="#8f00ff"
            />
            <TextInput
              style={styles.input}
              placeholder = "Password"
              placeholderTextColor="#8f00ff"
              secureTextEntry={true}
            />
          <View style={{flexDirection:'row'}}>    
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Biodata')}>
              <Text style={styles.teks}>LOGIN</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('')}>
              <Text style={styles.teks}>CANCEL</Text>
            </TouchableOpacity>
          </View>
            <StatusBar style="light" hidden={false} translucent={true} />
          <View style={{flexDirection:"row", marginTop:20}}>
            <Text style={{color:"#8f00ff", fontSize:12}}> Forgot Password? </Text>
              <TouchableOpacity onPress={() => navigation.navigate('Klik Here')}><Text style={{textDecorationLine:"underline", color:"#8f00ff", fontSize:12}}> Klik Here </Text></TouchableOpacity>
          </View>
        </View>
        </View>
      );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ccc0ce',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button:{
    alignItems:'center',
    justifyContent:'center',
    backgroundColor: '#8f00ff',    
    paddingHorizontal : 15,
    paddingVertical: 5,
    marginLeft:10,
    marginTop:30,
    borderRadius:50,
    height:40,
    width:100,
  },
  input:{
        borderColor:"purple",
        color:"purple",
        borderWidth:1,
        borderRadius: 30,
        marginVertical:10,
        paddingLeft:5,
        paddingRight:20,
        width:300,
        height:40,
  },
  teks:{
    color:"white",
  },
  logo:{
    alignItems:"center",
    width:310,
    height:90,
    marginBottom:50,
  }
});

export default Login;