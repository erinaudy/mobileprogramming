import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

class Biodata extends React.Component  {
    render(){
        return (
          <View style={styles.container}>
            <View style={{alignItems:'center',width:400, height:50, marginTop:-180, backgroundColor:'#e1d6f6'}}>
              <Text style={{color:'#8f00ff', fontSize:36}}> BIODATA MAHASISWA </Text>
            </View>
            <View style={styles.foto}>
            <Image
              source={require('./../assets/images/erinaaa.png')}          
              style={[styles.foto]}
            /> 
            </View>
            <View style={{alignItems:'center', width:200, height:30, marginTop:-10, marginLeft:-200, backgroundColor:'#8f00ff'}}>
              <Text style={{color:'#e1d6f6', fontSize:24, marginBottom:30, textAlign:'center'}}> Erina Undamayanti </Text>
            </View>
            <View style={{alignItems:'center', borderRadius:100, width:380, height:30, marginTop:80, marginLeft:-10, backgroundColor:'#e1d6f6'}}>
              <Text style={{color:'#8f00ff', fontSize:20, marginBottom:30, textAlign:'center'}}> TEKNIK INFORMATIKA </Text>
              <Image
                style={[styles.prodi]}
                source={require('./../assets/images/prodi.png')}          
              /> 
            </View>
            <View style={{alignItems:'center', borderRadius:100, width:380, height:30, marginTop:20, marginLeft:-10, backgroundColor:'#8f00ff'}}>
              <Text style={{color:'#e1d6f6', fontSize:20, marginBottom:30}}> PAGI A </Text>
              <Image
                style={[styles.kelas]}
                source={require('./../assets/images/kelas.png')}          
              /> 
            </View>
            <View style={{alignItems:'center', borderRadius:100, width:380, height:30, marginTop:20, marginLeft:-10, backgroundColor:'#e1d6f6'}}>
              <Text style={{color:'#8f00ff', fontSize:20, marginBottom:30, textAlign:'center'}}> STT WASTUKANCA PURWAKARTA </Text>
              <Image
                style={[styles.kampus]}
                source={require('./../assets/images/kampus.png')}          
              /> 
            </View>
            <View style={{position:'absolute', alignSelf:'flex-end', top:'25%'}}>
              <View style={{alignItems:'center', width:150, height:30, backgroundColor:'#3b5998'}}>
                <Text style={{color:'#e1d6f6', fontSize:18, marginBottom:30, alignItems:'center'}}> @Erina Udy </Text>
                <Image
                  style={[styles.facebook]}
                  source={require('./../assets/images/facebook.png')}          
                /> 
              </View>
              <View style={{alignItems:'center', width:150, height:30, marginTop:5, backgroundColor:'#00acee'}}>
                <Text style={{color:'#e1d6f6', fontSize:18, marginBottom:30, alignSelf:'flex-end'}}> @erinaundmynti </Text>
                <Image
                  style={[styles.twitter]}
                  source={require('./../assets/images/twitter.png')}          
                /> 
              </View>
              <View style={{alignItems:'center', width:150, height:30, marginTop:5, backgroundColor:'#ff6699'}}>
                <Text style={{color:'#e1d6f6', fontSize:18, marginBottom:30, alignSelf:'flex-end'}}> @erinaundmynti </Text>
                <Image
                  style={[styles.instagram]}
                  source={require('./../assets/images/instagram.png')}          
                /> 
              </View>
            </View>
            
        </View>   
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#ccc0ce',
      alignItems: 'center',
      justifyContent: 'center',
    },
    foto:{
        width:200,
        height:250,
        marginLeft:-70,
        marginTop:-10,
        resizeMode:'stretch',
    },
    prodi:{
        width:22,
        height:22,
        marginLeft:-300,
        marginTop:-52,
    },
    kelas:{
      width:22,
      height:22,
      marginLeft:-300,
      marginTop:-52,
    },
    kampus:{
      width:22,
      height:22,
      marginLeft:-300,
      marginTop:-50,
    },
    facebook:{
      width:20,
      height:20,
      marginLeft:-120,
      marginTop:-48,
    },
    twitter:{
      width:18,
      height:18,
      marginLeft:-120,
      marginTop:-48,
    },
    instagram:{
      width:18,
      height:18,
      marginLeft:-120,
      marginTop:-48,
    }

  
});

export default Biodata;