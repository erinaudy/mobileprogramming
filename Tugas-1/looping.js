// Looping

// A. Looping While
{
    console.log (' LOOPING PERTAMA ')
    var teks = 1
    while (teks <= 20)
    {
        if (teks%2==0)
        {
            console.log (teks + ' - I Love Coding')
        }
        teks++
    }
    console.log('\n')

    console.log(' LOOPING KEDUA ')
    var teks2 = 20
    while(teks2 >= 1)
    {
        if (teks2%2==0)
        {
            console.log(teks2 + ' -  I will become a mobile developer')
        }
        teks2--
    }
    console.log('\n')
}

// B. Looping menggunakan For
{
    console.log(' LOOPING MENGGUNAKAN FOR ')

    for (var a=1; a<=20; a++)
    {
        if (a%3==0)
        {
            if (a%2==0)
            {
                console.log(a + ' - Informatika')
            }
            else
            {
                console.log(a + ' - I Love Coding')
            }
        }

        else if (a%2==0)
        {
            console.log(a + ' - Informatika')
        }
        else
        {
            console.log (a + ' - Teknik')
        }
    }
    console.log('\n')
}

// C. Membuat Persegi Panjang 
{
    console.log('MEMBUAT PERSEGI PANJANG')

    var p='';
    for (var a=0; a<4; a++)
    {
        for (var b=0; b<8; b++)
        {
            p = p + '#';
        }
        p = p + '\n';
    }
    console.log(p);

    console.log('\n')
}

 // D. Membuat Tangga
{
    console.log('MEMBUAT TANGGA')

    var s='';
    for (var c=0; c<7; c++)
    {
        for (var d=0; d<=c; d++)
        {
            s= s + '#';
        }
        s= s + '\n';
    }
    console.log(s);

    console.log('\n')
} 

// E. Membuat Papan Catur
{
    console.log('MEMBUAT PAPAN CATUR')

    var catur =''
    for (var baris = 1; baris<=8; baris++)
    {
        if (baris%2==1)
        {
            for (var papan=0; papan<8; papan++)
            {
                if (papan%2==1)
                {
                    catur = catur + '#'
                }
                else
                {
                    catur = catur + ' '
                }
            }
        }
        else
        {
            for (var papan=0; papan<8; papan++)
            {
                if (papan%2==0)
                {
                    catur = catur + '#'
                }
                else
                {
                    catur = catur + ' '
                }
            }
        }
        catur = catur + '\n'
    }
    console.log(catur)
}