console.log('1. FUNCTION')
console.log()

// 1. Function Teriak
{
    console.log('1. Function Teriak')
    console.log()

    function teriak() {
        return 'Halo Humanika!'
    }
    console.log(teriak())

    console.log()
}
// 2. Function Kalikan
{
    console.log('2. Function Kalikan')
    console.log()

    function kalikan(num1_parameter, num2_parameter) {
        return num1_parameter * num2_parameter;
      }
      
      var num1 = 12;
      var num2 = 4;
      
      var hasilkali = kalikan(num1,num2);
      console.log(hasilkali);

    console.log()
}
// 3. Function Introduce
{
    console.log('3. Function Introduce')
    console.log()

    function introduce(name_p, age_p, address_p, hobby_p) {
        return "Nama saya "+ name_p + ", umur saya "+ age_p + " tahun"+ ", alamat saya di " + address_p + ", dan saya punya hobby yaitu "+ hobby_p +"!"
    }

    var name_p = "Agus"
    var age_p = 30
    var address_p = "Jln. Malioboro, Yogyakarta"
    var hobby_p = "Gaming"

    var perkenalan = introduce(name_p, age_p, address_p, hobby_p)
    console.log(perkenalan)
    
    console.log()
}