console.log('2. ARRAY')
console.log()

// 1. Range
{
    console.log('1. Range')
    console.log()

    function range(startNum, finishNum) {
        var number = []
        if (startNum < finishNum) {
            for (var a = startNum; a <= finishNum; a++) {
                number.push(a)
            }
        } 
        else if (startNum > finishNum) {
            for (var a = startNum; a >= finishNum; a--) {
                number.push(a)
            }
        } 
        else {
            number.push(-1)    
        }
        return number
    }
    console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    console.log(range(1)) // -1
    console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
    console.log(range(54, 50)) // [54, 53, 52, 51, 50]
    console.log(range()) // -1 

    console.log()
}

// 2. Range With Step
{
    console.log('2. Range Step')
    console.log()

    function rangeWithStep(startNum, finishNum, step){
        var number = []
        step = step -1

        if (startNum < finishNum){
            for (var a =startNum; a<=finishNum; a++){
            number.push(a)
            a = a + step
            }
            return number
        }
        else if (startNum > finishNum){
            for (var a=startNum; a>=finishNum; a--){
            number.push(a)
            a = a - step
            }
            return number
        }
        else {
            return -1
        }     
    }
    console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
    console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
    console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
    console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]
    
    console.log()
}

// 3. Sum Of Range
{
    console.log('3. Sum of Range')
    console.log()

    function sum(startNum, finishNum, step) {
        var  number = []
        
        if (startNum == null && finishNum == null && step == null) {
            number.push(0);
        }
        else if (startNum < finishNum) {
            if (step == null){
                step = 1
            }
            for (let a = startNum; a<=finishNum; a+=step) {
                number.push(a);
            }
        }
        else if (startNum > finishNum) {
            if (step == null) {
                step = 1
            }
            for (let a = startNum; a>=finishNum; a-=step) {
                number.push(a);
            }
        }
        else if (startNum == null || finishNum == null) {
            number.push(startNum);
        }
        let total = number.reduce((val, nilai) => {
            return val + nilai;
        }, 0)
        return total;
    }
    console.log(sum(1,10)) // 55
    console.log(sum(5, 50, 2)) // 621
    console.log(sum(15,10)) // 75
    console.log(sum(20, 10, 2)) // 90
    console.log(sum(1)) // 1
    console.log(sum()) // 0
    
    console.log()
}

// 4. Array Multidimensi
{
    console.log('4. Array Multidimensi')
    console.log()

    function dataHandling(biodata){
        for(var a = 0; a < biodata.length; a++){
            console.log("Nomor ID       : " + biodata[a][0]);
            console.log("Nama Lengkap   : " + biodata[a][1]);
            console.log("TTL            : " + biodata[a][2] + " " + biodata[a][3]);
            console.log("Hobi           : " + biodata[a][4]);
            console.log("");
        }
        return "";
    }
    
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ]
    console.log(dataHandling(input));
}

// 5. Balik Kata
{
    console.log('5. Balik Kata')
    console.log()

    function balikKata(string) {
        var currentString = string;
        var newString = '';
        for (let a = string.length - 1; a >= 0; a--) {
            newString = newString + currentString[a];
        }
        return newString;
    }
        console.log(balikKata("Kasur Rusak")) // kasuR rusaK
        console.log(balikKata("Informatika")) // akitamrofnI
        console.log(balikKata("Haji Ijah")) // hajI ijaH
        console.log(balikKata("racecar")) // racecar
        console.log(balikKata("I am Humanikers")) // srekinamuH ma I
        console.log()
}

// 6. Metode Array
{
    console.log('6. Metode Array')
    console.log()

    var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Memvbaca"];

    function dataHandling2(biodata){
        input.splice(1, 2, "Roman Alamsyah Elsharawy", "Provisinsi Bandar Lampung");
        input.splice(4, 1, "Pria", "SMA Internasional Metro");
        console.log(input)

        var inputSplit = input[3].split("/")
        var inputJoin = inputSplit.join("-")
        var bulan = inputJoin[3] + inputJoin[4]

        switch(bulan){
            case '01': console.log('Januari');
                break;
            case '02': console.log('Februari');
                break;
            case '03': console.log('Maret');
                break;
            case '04': console.log('April');
                break;
            case '05': console.log('Mei');
                break;
            case '06': console.log('Juni');
                break;
            case '07': console.log('Juli');
                break;
            case '08': console.log('Agustus');
                break;
            case '09': console.log('September');
                break;
            case '10' : console.log('Oktober');
                break;
            case '11': console.log('November');
                break;
            case '12': console.log('Desember');
                break;
     }
     
        var inputShortDes = inputSplit.sort(function (a,b){ return b - a});
        console.log(inputShortDes)

        var tes = input[3].split("/")
        console.log(tes.join("-"))

        console.log(input[1].toString().slice(0, 14))

        return biodata
    }
    dataHandling2();
}