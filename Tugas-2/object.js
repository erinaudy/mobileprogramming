console.log('3. OBJECT LITERAL')
console.log()

// 1. Array to Object
{
    console.log ('1. Array to Object')
    console.log()

    function arrayToObject(arr) {
        var arr = [
          ["Abduh", "Muhamad", "male", 1992], 
          ["Ahmad", "Taufik", "male", 1985]
        ]
        var people = [
          ["Bruce", "Banner", "male", 1975], 
          ["Natasha","Romanoff", "female"]
        ]
        var people2 = [
          ["Tony", "Stark", "male", 1980], 
          ["Pepper", "Pots", "female", 2023]
        ]
        var tampungarr = {}
        var tampungpeople = {}
        var tampungpeople2 = {}
      
        var now = new Date()
        var thisYear = now.getFullYear()
      
        console.log('Contoh')
        for (var a= 0; a < 2; a++) { 
          tampungarr.firstName = arr[a][0]
          tampungarr.lastName = arr[a][1]
          tampungarr.gender = arr[a][2] 
          tampungarr.age = thisYear - arr[a][3]
          console.log(a + 1 + '.' + ' ' + tampungarr.firstName + ' ' + tampungarr.lastName + ': ' + '{ ' + 'firstName: ' + '"' + tampungarr.firstName + '"' + ', ' + 'lastName: ' + '"' + tampungarr.lastName + '"' + ', ' + 'gender: ' + '"' + tampungarr.gender + '"' + ', ' + 'age: ' + tampungarr.age + '}');
        }
        console.log()
        
        console.log('People')
        for (var b= 0; b < 2; b++) {
          tampungpeople.firstName = people[b][0]
          tampungpeople.lastName = people[b][1]
          tampungpeople.gender = people[b][2] 
          tampungpeople.age = thisYear - people[b][3]
          if (tampungpeople.age = thisYear - people[b][3]) {
            console.log(b + 1 + '.' + ' ' + tampungpeople.firstName + ' ' + tampungpeople.lastName + ': ' + '{ ' + 'firstName: ' + '"' + tampungpeople.firstName + '"' + ', ' + 'lastName: ' + '"' + tampungpeople.lastName + '"' + ', ' + 'gender: ' + '"' + tampungpeople.gender + '"' + ', ' + 'age: ' + tampungpeople.age + '}'); 
          } else {
            console.log(b + 1 + '.' + ' ' + tampungpeople.firstName + ' ' + tampungpeople.lastName + ': ' + '{ ' + 'firstName: ' + '"' + tampungpeople.firstName + '"' + ', ' + 'lastName: ' + '"' + tampungpeople.lastName + '"' + ', ' + 'gender: ' + '"' + tampungpeople.gender + '"' + ', ' + 'age: ' + '"Invalid Birth Year"' + '}');     
          }
        }
        console.log()
      
        console.log('People2')
        for (var c= 0; c < 2; c++) {
          tampungpeople2.firstName = people2[c][0]
          tampungpeople2.lastName = people2[c][1]
          tampungpeople2.gender = people2[c][2]
          if (tampungpeople2.age = thisYear < people2[c][3]) {
            console.log(c + 1 + '.' + ' ' + tampungpeople2.firstName + ' ' + tampungpeople2.lastName + ': ' + '{ ' + 'firstName: ' + '"' + tampungpeople2.firstName + '"' + ', ' + 'lastName: ' + '"' + tampungpeople2.lastName + '"' + ', ' + 'gender: ' + '"' + tampungpeople2.gender + '"' + ', ' + 'age: ' + '"Invalid Birth Year"' + '}');
          } else {
            tampungpeople2.age = thisYear - people2[c][3]
            console.log(c + 1 + '.' + ' ' + tampungpeople2.firstName + ' ' + tampungpeople2.lastName + ': ' + '{ ' + 'firstName: ' + '"' + tampungpeople2.firstName + '"' + ', ' + 'lastName: ' + '"' + tampungpeople2.lastName + '"' + ', ' + 'gender: ' + '"' + tampungpeople2.gender + '"' + ', ' + 'age: ' + tampungpeople2.age + '}');     
          }
        }
      }
      console.log(arrayToObject())
    console.log()
}

// 2. Shopping Time
{
    console.log('2. Shopping Time')
    console.log()

    function shoppingTime(memberId, money) {
        var shopping = {}
        var barang = [
                        ['Sepatu Stacattu', 1500000],
                        ['Baju Zoro', 500000],
                        ['Baju H&N', 250000],
                        ['Sweater Uniklooh', 175000],
                        ['Casing Handphone', 50000]
                     ]
        
        if(memberId === undefined && money === undefined){
          return 'Mohon maaf, toko X hanya berlaku untuk member saja'
        }
        
        if(memberId === ''){
          return "Mohon maaf, toko X hanya berlaku untuk member saja"
        } else {
          shopping.memberId = memberId
        }
        
        if(money <= 50000 ){
          return 'Mohon maaf, uang tidak cukup'
        } else {
          shopping.money = money
        }
        
        var jumlahHargaBarang = 0
        shopping.listPurchased = []
        for(let a = 0; a < barang.length; a++){
          
          if(money > barang[a][1]){
            shopping.listPurchased.push(barang[a][0])
            jumlahHargaBarang += barang[a][1]
          }
          shopping.changeMoney = money - jumlahHargaBarang
        }
         return shopping
      }
      
      // TEST CASES
      console.log(shoppingTime('1820RzKrnWn08', 2475000));
        //{ memberId: '1820RzKrnWn08',
        // money: 2475000,
        // listPurchased:
        //  [ 'Sepatu Stacattu',
        //    'Baju Zoro',
        //    'Baju H&N',
        //    'Sweater Uniklooh',
        //    'Casing Handphone' ],
        // changeMoney: 0 }
      console.log(shoppingTime('82Ku8Ma742', 170000));
      //{ memberId: '82Ku8Ma742',
      // money: 170000,
      // listPurchased:
      //  [ 'Casing Handphone' ],
      // changeMoney: 120000 }
      console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
      console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
      console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
    console.log()
}

// 3. Naik Angkot
{
    console.log('3. Naik Angkot')
    console.log()

    function naikAngkot(arrPenumpang) {
        rute = ['A', 'B', 'C', 'D', 'E', 'F'];
            var angkot = [{},{}];
            var asal = '';
            var tujuan = '';
      
        for (var a=0; a<arrPenumpang.length; a++) {
      
            for (var b = 0; b<arrPenumpang[a].length; b++) {
                switch (b) {
                    case 0: {
                        angkot[a].penumpang = arrPenumpang[a][b];
                        break;
                    } 
                    case 1: {
                        angkot[a].naikDari = arrPenumpang[a][b];
                        angkot[a].tujuan = arrPenumpang[a][b+1];
                        break;
                    } 
                    case 2: {
                        asal = arrPenumpang[a][b-1];
                        tujuan = arrPenumpang[a][b];
                        var jarak = 0;
                        for (var k=0; k<rute.length; k++) {
                            if (rute[k] === asal) {
                                for (var l=k+1; l<rute.length; l++) {
                                    jarak += 1;
                                    if (rute[l] === tujuan) {
                                        var bayar = jarak * 2000;
                                        angkot[a].bayar = bayar;
                                    }
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }
      return angkot;
    }
      //TEST CASE
      console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']])); 
      // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
      //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
      
        console.log(naikAngkot([])); //[]
      
    console.log()
}