console.log('SOAL 2. CLASS')
console.log()

// 1. Animal Class
{
    console.log('1. Animal Class')
    console.log()

    class Animal {
        constructor(name) {
            this.name = name
            this.legs = 4
            this.cold_blooded = false
        }
        get nam() {
            return this.name
        }
        set nam(n) {
            return this.name = n
        }
        get leg() {
            return this.legs
        }
        set leg(l) {
            return this.legs = l
        }
        get cold_blood() {
            return this.cold_blooded
        }
        set cold_blood(c) {
            return this.cold_blooded = c
        }
    }
    class Ape extends Animal{
        constructor(name, legs, cold_blooded,) {
            super(name, cold_blooded)
            this.legs = 2
        }
        yell(){
            return "Auooo"
        }
    }
    class Frog extends Animal {
        constructor(name, legs, cold_blooded) {
            super(name, legs, cold_blooded)
        }
        jump(){
            return "hop hop"
        }
    }
       let sheep = new Animal("shaun");
       console.log(sheep.nam) // "shaun"
       console.log(sheep.leg) // 4
       console.log(sheep.cold_blood) // false
       console.log()

        var sungokong = new Ape("kera sakti")
        console.log(sungokong.name) // "kera sakti"
        console.log(sungokong.legs) // 2
        console.log(sungokong.cold_blood) // false
        console.log(sungokong.yell())// "Auooo"
        console.log()

        var kodok = new Frog("buduk")
        console.log(kodok.name) // "buduk"
        console.log(kodok.leg) // 4
        console.log(kodok.cold_blood) // false
        console.log(kodok.jump())  // "hop hop"


    console.log()
}

// 2. Fuction to Class
{
    console.log('2. Function to Class')
    console.log()

    class Clock {
        constructor ({ template }) {
            this.template = template;
        }
        render() {
        let date = new Date();
        
        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
        
        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
        
        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        
        let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
        
        console.log(output);
        }
        stop() {
            clearInterval(timer);
        };
        start() {
            this.render();
            this.timer = setInterval(() => this.render(),1000);
        }
    }
       let clock = new Clock({template: 'h:m:s'});
       clock.start(); 

    console.log()

}