console.log('SOAL 1. ES6')
console.log()

// 1. Mengubah Fungsi Menjadi Fungsi Arrow
{
    console.log('1. Mengubah Fungsi Menjadi Fungsi Arrow')
    console.log()

    var golden = () => {
        console.log('this is golden!!')
    }
    golden();

    console.log()
}

// 2. Sederhanakan Menjadi Objek Literal di ES6
{
    console.log('1. Sederhanakan Menjadi Object Literal di ES6')
    console.log()

    newFunction = (firstName, lastName) => {
        firstName
        lastName
        return {
            fullName() {
                return console.log(firstName + " " + lastName)
            }
        }
    }
    newFunction ("William", "Imoh").fullName();

    console.log()
}

// 3. Destructuring
{
    console.log('3. Destructuring')
    console.log()

    const newObject = {
        firstName : "Harry",
        lastName : "Potter Holt",
        destination : "Hogwarts React Conf",
        occupation : "Deve-wizard Avocado",
        spell : "Vimulus Renderus!!!"
    }

    const {firstName, lastName, destination, occupation, spell} = newObject;
    console.log(firstName, lastName, destination, occupation)

    console.log()
}

//  4. Array Spreading
{
    console.log('4. Array Spreading')
    console.log()

    const west = ["Will", "Chris", "Sam", "Holly"]
    const east = ["Gill", "Brian", "Noel", "Maggie"]

    let combined = [west,east];
    console.log(combined)

    console.log()
}

// 5. Template Literals
{
    console.log('5. Template Literals')
    console.log()
    
    const planet = "earth"
    const view = "glass"

    const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
    console.log(before);

    console.log()
}